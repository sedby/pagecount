<?php
/**
 * Russian Language File for pagecount Plugin
 * @version 1.00
 * @author Aliaksei Kobak
 * @copyright (c) seditio.by 2014-2015
 */

defined('COT_CODE') or die('Wrong URL');

$L['Pagecount'] = 'Счетчик страниц';
$L['info_desc'] = 'Вывод количества страниц на сайте в соответствии с условиями';