<?php
/**
 * English Language File for Pagecount Plugin
 * @version 1.00
 * @author Aliaksei Kobak
 * @copyright (c) seditio.by 2014-2015
 */

defined('COT_CODE') or die('Wrong URL');

$L['Pagecount'] = 'Pagecount Plugin';
$L['info_desc'] = 'Displays number of pages according to conditions';