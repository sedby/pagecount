<?php
/* ====================
[BEGIN_COT_EXT]
Hooks=global
[END_COT_EXT]
==================== */

/**
 * @package Pagecount
 * @version 1.00
 * @author Aliaksei Kobak
 * @copyright (c) seditio.by 2014-2015
 */

defined('COT_CODE') or die('Wrong URL');

require_once cot_incfile('page', 'module');
require_once cot_langfile('pagecount', 'plug');

function pagecount ( $cat = '__root__', $whitelist = '', $blacklist = '', $sqlq = '', $sub = true, $decl = '' )
{
	global $db, $db_pages, $db_users, $env, $structure, $cot_extrafields, $Ls;

	if( $cat == '__root__' )
	{
        $totalitems = $db->query("SELECT COUNT(*)
		FROM $db_pages AS p 
		WHERE page_state='0' ")->fetchColumn();    
	}
	else
	{
// Compile lists
		if (!empty($blacklist))
		{
			$bl = explode(';', $blacklist);
		}
		if (!empty($whitelist))
		{
			$wl = explode(';', $whitelist);
		}
// Get the cats
		$cats = array();
		if (empty($cat) && (!empty($blacklist) || !empty($whitelist)))
		{
// All cats except bl/wl
			foreach ($structure['page'] as $code => $row)
			{
				if (!empty($blacklist) && !in_array($code, $bl) || !empty($whitelist) && in_array($code, $wl))
				{
					$cats[] = $code;
				}
			}
		}
		elseif (!empty($cat) && $sub)
		{
// Specific cat
			$cats = cot_structure_children('page', $cat, $sub);
		}

		if (count($cats) > 0)
		{
			if (!empty($blacklist))
			{
				$cats = array_diff($cats, $bl);
			}
			if (!empty($whitelist))
			{
				$cats = array_intersect($cats, $wl);
			}
			$where_cat = "AND page_cat IN ('" . implode("','", $cats) . "')";
		}
		elseif (!empty($cat))
		{
			$where_cat = "AND page_cat = " . $db->quote($cat);
		}	
		if (!empty($sqlq))
		{
			$sqlq = "AND $sqlq";
		}	

		$totalitems = $db->query("SELECT COUNT(*)
		FROM $db_pages AS p 
		WHERE page_state='0' $where_cat $sqlq")->fetchColumn();

		if (!empty($decl))
		{
			$totalitems = cot_declension($totalitems, $Ls[$decl]);
		}
	}
	return ( $totalitems );
}