<?php
/* ====================
[BEGIN_COT_EXT]
 * Code=pagecount
 * Name=Page Count Widget
 * Category=navigation-structure
 * Description=Displays number of pages accoding to conditions
 * Version=1.00
 * Date=2015-03-26
 * Author=Aliaksei Kobak
 * Copyright=&copy; 2014-2015 seditio.by
 * Notes=
 * Auth_guests=R
 * Lock_guests=12345A
 * Auth_members=RW
 * Lock_members=12345A
[END_COT_EXT]
[BEGIN_COT_EXT_CONFIG]
[END_COT_EXT_CONFIG]
==================== */

/**
 * @package Pagecount
 * @version 1.00
 * @author Aliaksei Kobak
 * @copyright (c) seditio.by 2014-2015
 */
 
defined('COT_CODE') or die('Wrong URL');